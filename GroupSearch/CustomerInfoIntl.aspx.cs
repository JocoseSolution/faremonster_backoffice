﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using GRP_Booking;
public partial class GroupSearch_CustomerInfoIntl : System.Web.UI.Page
{
    int AdultCount, ChildCount, InfantCount;
    DataSet PaxCountDS = new DataSet();
    GroupBooking ObjGB = new GroupBooking();
    SqlTransactionDom ObjST = new SqlTransactionDom();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null || Session["UID"].ToString() == "")
            {
                Response.Redirect("../GroupBookingLogin.aspx");
            }
            else if (Page.IsPostBack != true)
            {
                if (Request.QueryString["RefRequestID"] != null || Request.QueryString["RefRequestID"] != "")
                {
                    PaxCountDS = ObjGB.GroupRequestDetails(Request.QueryString["RefRequestID"], "PAXCOUNTINFO", "", "");
                    AdultCount = Convert.ToInt32(PaxCountDS.Tables[0].Rows[0]["AdultCount"].ToString());
                    ChildCount = Convert.ToInt32(PaxCountDS.Tables[0].Rows[0]["ChildCount"].ToString());
                    InfantCount = Convert.ToInt32(PaxCountDS.Tables[0].Rows[0]["InfantCount"].ToString());
                    Bind_pax(AdultCount, ChildCount, InfantCount);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLogTrace.WriteErrorLog(ex,"CoustomerInfoIntlPageLoad");
        }
    }
    public void Bind_pax(int cntAdult, int cntChild, int cntInfant)
    {
        try
        {
            DataTable PaxTbl = new DataTable();
            DataColumn cntTblColumn = null;
            cntTblColumn = new DataColumn();
            cntTblColumn.DataType = Type.GetType("System.Double");
            cntTblColumn.ColumnName = "Counter";
            PaxTbl.Columns.Add(cntTblColumn);
            cntTblColumn = new DataColumn();
            cntTblColumn.DataType = Type.GetType("System.String");
            cntTblColumn.ColumnName = "PaxTP";
            PaxTbl.Columns.Add(cntTblColumn);
            DataRow cntrow = null;
            for (int i = 1; i <= cntAdult; i++)
            {
                cntrow = PaxTbl.NewRow();
                cntrow["Counter"] = i;
                cntrow["PaxTP"] = "Passenger " + i.ToString() + " (Adult)";
                PaxTbl.Rows.Add(cntrow);
            }
            Repeater_Adult.DataSource = PaxTbl;
            Repeater_Adult.DataBind();
            PaxTbl.Clear();
            if (cntChild > 0)
            {
                for (int i = 1; i <= cntChild; i++)
                {
                    cntrow = PaxTbl.NewRow();
                    cntrow["Counter"] = i;
                    cntrow["PaxTP"] = "Passenger " + i.ToString() + " (Child)";
                    PaxTbl.Rows.Add(cntrow);
                }
                Repeater_Child.DataSource = PaxTbl;
                Repeater_Child.DataBind();
            }
            PaxTbl.Clear();
            if (cntInfant > 0)
            {
                for (int i = 1; i <= cntInfant; i++)
                {
                    cntrow = PaxTbl.NewRow();
                    cntrow["Counter"] = i;
                    cntrow["PaxTP"] = "Passenger " + i.ToString() + " (Infant)";
                    PaxTbl.Rows.Add(cntrow);
                }
                Repeater_Infant.DataSource = PaxTbl;
                Repeater_Infant.DataBind();
            }
        }
        catch (Exception ex)
        {
            ErrorLogTrace.WriteErrorLog(ex, "CoustomerInfoIntlBindPax");
        }
    }
    protected void book_Click(object sender, EventArgs e)
    {
        try
        {
            if ((Request.QueryString["RefRequestID"] != null || Request.QueryString["RefRequestID"] != "") && Request.QueryString["Status"] == "PAID")
            {
                InsertPaxDetail(Request.QueryString["RefRequestID"]);
                int j = 0, k = 0;
                j = AgentMailSending(Request.QueryString["RefRequestID"]);
                k = AdminNExecMailSending("Payment has been done by the agent, please provide the Ticket /Pnr no", Request.QueryString["RefRequestID"]);
                if (j == 1 && k == 1)
                {
                    
                    string linktype = Request.QueryString["Payment"].ToString();
                    if (linktype.ToLower() == "off")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Payment has been done, will get back to you soon');window.location ='../GroupSearch/GroupDetails.aspx?RefRequestID=" + Request.QueryString["RefRequestID"] + "';", true);
                    }
                    else if (linktype.ToLower() == "on" || linktype.ToLower() == "pg")
                    {
                        if (Request.QueryString["PG"].ToString() == "Y")
                        {
                            ObjGB.UPDATEPAYMENTSTATUS(Request.QueryString["RefRequestID"], Session["UID"].ToString(), "UPDATED", "PG");
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Payment has been done succesfully,will get back to you soon!!');window.opener.location.reload(true);self.close();", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc(3);", true);
            }
        }
        catch (Exception ex)
        {
            ErrorLogTrace.WriteErrorLog(ex, "CoustomerInfoIntlBookClick");
        }
    }
    public void InsertPaxDetail(string trackid)
    {
        try
        {
            PaxCountDS = ObjGB.GroupRequestDetails(trackid, "PAXCOUNTINFO", "", "");
            if (PaxCountDS.Tables[0].Rows.Count > 0)
            {
                AdultCount = Convert.ToInt32(PaxCountDS.Tables[0].Rows[0]["AdultCount"].ToString());
                ChildCount = Convert.ToInt32(PaxCountDS.Tables[0].Rows[0]["ChildCount"].ToString());
                InfantCount = Convert.ToInt32(PaxCountDS.Tables[0].Rows[0]["InfantCount"].ToString());
                int counter = 0;
                foreach (RepeaterItem rw in Repeater_Adult.Items)
                {
                    counter += 1;
                    DropDownList ddl_ATitle = (DropDownList)rw.FindControl("ddl_ATitle");
                    DropDownList ddl_AGender = (DropDownList)rw.FindControl("ddl_AGender");
                    TextBox txtAFirstName = (TextBox)rw.FindControl("txtAFirstName");
                    TextBox txtAMiddleName = (TextBox)rw.FindControl("txtAMiddleName");
                    if (txtAMiddleName.Text == "Middle Name")
                    {
                        txtAMiddleName.Text = "";
                    }
                    TextBox txtALastName = (TextBox)rw.FindControl("txtALastName");
                    TextBox txtadultDOB = (TextBox)rw.FindControl("Txt_AdtDOB");
                    string DOB = "";
                    DOB = txtadultDOB.Text.Trim();
                    string gender = null;
                    gender = ddl_AGender.SelectedValue.Trim();
                    ///''''''''''''''''''''' for adlut Possport/and all '''''''''''''''''''''''''''''''''''
                    HtmlInputHidden IssuingCountry_Adl = (HtmlInputHidden)rw.FindControl("Hdn_IssuingCountry_Adl");
                    HtmlInputHidden Nationality_Adl = (HtmlInputHidden)rw.FindControl("Hdn_Nationality_Adl");
                    TextBox Passport_Adl = (TextBox)rw.FindControl("txt_Passport_Adl");
                    TextBox Ex_date_Adl = (TextBox)rw.FindControl("txt_ex_date_Adl");
                    ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    DropDownList ddl_AMealPrefer = (DropDownList)rw.FindControl("ddl_AMealPrefer");
                    DropDownList ddl_ASeatPrefer = (DropDownList)rw.FindControl("ddl_ASeatPrefer");
                    HtmlInputHidden txt_AAirline = (HtmlInputHidden)rw.FindControl("hidtxtAirline_int");
                    if (counter <= InfantCount)
                    {
                        ObjGB.GRP_InsertPaxDetails(trackid, ddl_ATitle.SelectedValue.Trim(), txtAFirstName.Text.Trim(), txtAMiddleName.Text.Trim(), txtALastName.Text.Trim(), "ADT", DOB, gender, Ex_date_Adl.Text, Passport_Adl.Text, IssuingCountry_Adl.Value, Nationality_Adl.Value, "INT", "Y");
                    }
                    else
                    {
                        ObjGB.GRP_InsertPaxDetails(trackid, ddl_ATitle.SelectedValue.Trim(), txtAFirstName.Text.Trim(), txtAMiddleName.Text.Trim(), txtALastName.Text.Trim(), "ADT", DOB, gender, Ex_date_Adl.Text, Passport_Adl.Text, IssuingCountry_Adl.Value, Nationality_Adl.Value, "INT", "Y");
                    }
                }
                if (ChildCount > 0)
                {
                    foreach (RepeaterItem rw in Repeater_Child.Items)
                    {
                        DropDownList ddl_CTitle = (DropDownList)rw.FindControl("ddl_CTitle");
                        DropDownList ddl_CGender = (DropDownList)rw.FindControl("ddl_CGender");
                        TextBox txtCFirstName = (TextBox)rw.FindControl("txtCFirstName");
                        TextBox txtCMiddleName = (TextBox)rw.FindControl("txtCMiddleName");
                        if (txtCMiddleName.Text == "Middle Name")
                        {
                            txtCMiddleName.Text = "";
                        }
                        TextBox txtCLastName = (TextBox)rw.FindControl("txtCLastName");
                        TextBox txtchildDOB = (TextBox)rw.FindControl("Txt_chDOB");
                        string DOB = "";
                        DOB = txtchildDOB.Text.Trim();
                        string gender = null;
                        gender = ddl_CGender.SelectedValue.Trim();
                        ///''''''''''''''''''''' for Child Possport/and all '''''''''''''''''''''''''''''''''''
                        TextBox Passport_Chd = (TextBox)rw.FindControl("txt_Passport_Chd");
                        TextBox Ex_date_Chd = (TextBox)rw.FindControl("txt_ex_date_Chd");
                        HtmlInputHidden IssuingCountry_Chd = (HtmlInputHidden)rw.FindControl("Hdn_IssuingCountry_Chd");
                        HtmlInputHidden Nationality_Chd = (HtmlInputHidden)rw.FindControl("Hdn_Nationality_Chd");
                        ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        DropDownList ddl_CMealPrefer = (DropDownList)rw.FindControl("ddl_CMealPrefer");
                        DropDownList ddl_CSeatPrefer = (DropDownList)rw.FindControl("ddl_CSeatPrefer");
                        ObjGB.GRP_InsertPaxDetails(trackid, ddl_CTitle.SelectedValue.Trim(), txtCFirstName.Text.Trim(), txtCMiddleName.Text.Trim(), txtCLastName.Text.Trim(), "CHD", DOB, gender, Ex_date_Chd.Text, Passport_Chd.Text, IssuingCountry_Chd.Value, Nationality_Chd.Value, "INT", "Y");
                    }
                }
                if (InfantCount > 0)
                {
                    int counter1 = 0;
                    foreach (RepeaterItem rw in Repeater_Infant.Items)
                    {
                        DropDownList ddl_ITitle = (DropDownList)rw.FindControl("ddl_ITitle");
                        DropDownList ddl_IGender = (DropDownList)rw.FindControl("ddl_IGender");
                        TextBox txtIFirstName = (TextBox)rw.FindControl("txtIFirstName");
                        TextBox txtIMiddleName = (TextBox)rw.FindControl("txtIMiddleName");
                        if (txtIMiddleName.Text == "Middle Name")
                        {
                            txtIMiddleName.Text = "";
                        }
                        TextBox txtILastName = (TextBox)rw.FindControl("txtILastName");
                        TextBox txtinfantDOB = (TextBox)rw.FindControl("Txt_InfantDOB");
                        string DOB = "";
                        DOB = txtinfantDOB.Text.Trim();
                        string gender = null;
                        gender = ddl_IGender.SelectedValue.Trim();
                        ///''''''''''''''''''''' for Infant Possport/and all '''''''''''''''''''''''''''''''''''
                        TextBox Passport_Inf = (TextBox)rw.FindControl("txt_Passport_Inf");
                        TextBox Ex_date_Inf = (TextBox)rw.FindControl("txt_ex_date_Inf");
                        HtmlInputHidden IssuingCountry_Inf = (HtmlInputHidden)rw.FindControl("Hdn_IssuingCountry_Inf");
                        HtmlInputHidden Nationality_Inf = (HtmlInputHidden)rw.FindControl("Hdn_Nationality_Inf");
                        ///'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        TextBox txtAFirstName = (TextBox)Repeater_Adult.Items[counter1].FindControl("txtAFirstName");
                        TextBox txtAMiddleName = (TextBox)Repeater_Adult.Items[counter1].FindControl("txtAMiddleName");
                        TextBox txtALastName = (TextBox)Repeater_Adult.Items[counter1].FindControl("txtALastName");
                        string Name = "";
                        Name = txtAFirstName.Text.Trim() + txtAMiddleName.Text.Trim() + txtALastName.Text.Trim();
                        if (counter1 <= InfantCount)
                        {
                            ObjGB.GRP_InsertPaxDetails(trackid, ddl_ITitle.SelectedValue.Trim(), txtIFirstName.Text.Trim(), txtIMiddleName.Text.Trim(), txtILastName.Text.Trim(), "INF", DOB, gender, Ex_date_Inf.Text, Passport_Inf.Text, IssuingCountry_Inf.Value, Nationality_Inf.Value, "INT", "Y");
                        }
                        counter1 += 1;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLogTrace.WriteErrorLog(ex, "CoustomerInfoIntlInsertPax");
        }
    }
    protected int AgentMailSending(string RefRequestID)
    {
        int i = 0;
        try
        {
            DataSet MailDt = new DataSet();
            DataSet MailContent = new DataSet();
            DataSet DSStatus = new DataSet();
            string StrMail = "";
            string strMailMsg = "";
            MailContent = ObjGB.GroupRequestDetails(RefRequestID, "MAILCONTENTAGENT", "", "");
            DSStatus = ObjGB.SENDPAYMENTLINK(RefRequestID, Session["UID"].ToString());
            if (DSStatus.Tables[0].Rows.Count > 0)
            {
                string MSG = "Payment has been done!!";
                StrMail = ObjGB.GETEMAILID(RefRequestID, "AGENT");
                strMailMsg = "<table>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><h2> Group Booking Details </h2>";
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td> Congrates, " + MSG + "";
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b>Request ID: </b>" + MailContent.Tables[0].Rows[0]["RequestID"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b>Trip: </b>" + MailContent.Tables[0].Rows[0]["Trip"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b>TripType: </b>" + MailContent.Tables[0].Rows[0]["TripType"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b>Numbers of Adult: </b>" + MailContent.Tables[0].Rows[0]["AdultCount"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                if (Convert.ToInt32(MailContent.Tables[0].Rows[0]["ChildCount"].ToString()) > 0)
                {
                    strMailMsg = strMailMsg + "<tr>";
                    strMailMsg = strMailMsg + "<td><b>Numbers of Child: </b>" + MailContent.Tables[0].Rows[0]["ChildCount"].ToString();
                    strMailMsg = strMailMsg + "</td>";
                    strMailMsg = strMailMsg + "</tr>";
                }
                if (Convert.ToInt32(MailContent.Tables[0].Rows[0]["InfantCount"].ToString()) > 0)
                {
                    strMailMsg = strMailMsg + "<tr>";
                    strMailMsg = strMailMsg + "<td><b>Numbers of Infant: </b>" + MailContent.Tables[0].Rows[0]["InfantCount"].ToString();
                    strMailMsg = strMailMsg + "</td>";
                    strMailMsg = strMailMsg + "</tr>";
                }
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b> Your Expacted Fare: </b>" + MailContent.Tables[0].Rows[0]["ExpactedPrice"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b> Booking Fare: </b>" + DSStatus.Tables[0].Rows[0]["BookedPrice"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
                strMailMsg = strMailMsg + "</table>";
                MailDt = ObjST.GetMailingDetails("GroupBooking", "All");
                if (MailDt.Tables[0].Rows.Count > 0)
                {
                    string EIDStrMail = ConfigurationManager.AppSettings["EmailId"].ToString();
                    if (StrMail != "")
                    {
                        i = ObjGB.SendMail(StrMail, MailDt.Tables[0].Rows[0]["MAILFROM"].ToString(), MailDt.Tables[0].Rows[0]["CC"].ToString(), "", MailDt.Tables[0].Rows[0]["SMTPCLIENT"].ToString(), MailDt.Tables[0].Rows[0]["UserID"].ToString(), MailDt.Tables[0].Rows[0]["Pass"].ToString(), strMailMsg, "Group Booking Details", "");
                    }
                    else
                    {
                        i = ObjGB.SendMail(EIDStrMail, MailDt.Tables[0].Rows[0]["MAILFROM"].ToString(), MailDt.Tables[0].Rows[0]["CC"].ToString(), "", MailDt.Tables[0].Rows[0]["SMTPCLIENT"].ToString(), MailDt.Tables[0].Rows[0]["UserID"].ToString(), MailDt.Tables[0].Rows[0]["Pass"].ToString(), strMailMsg, "Agent Email-ID not found (Group Booking Details)", "");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLogTrace.WriteErrorLog(ex, "CoustomerInfoIntlAgentMail");
        }
        return i;
    }
    protected int AdminNExecMailSending(string MailMSG, string RefRequestID)
    {
        int i = 0;
        try
        {
            DataSet MailDt = new DataSet();
            DataSet MailContent = new DataSet();
            string StrMail = "";
            string strMailMsg = "";
            MailContent = ObjGB.GroupRequestDetails(RefRequestID, "MAILCONTENTAGENT", "", "");
            StrMail = ObjGB.GETEMAILID(RefRequestID, "EXEC");
            strMailMsg = "<table>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><h2> Group Booking Details </h2>";
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td>";
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><b>Request ID: </b>" + MailContent.Tables[0].Rows[0]["RequestID"].ToString();
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><b>Trip: </b>" + MailContent.Tables[0].Rows[0]["Trip"].ToString();
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><b>TripType: </b>" + MailContent.Tables[0].Rows[0]["TripType"].ToString();
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><b>Numbers of Adult: </b>" + MailContent.Tables[0].Rows[0]["AdultCount"].ToString();
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            if (Convert.ToInt32(MailContent.Tables[0].Rows[0]["ChildCount"].ToString()) > 0)
            {
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b>Numbers of Child: </b>" + MailContent.Tables[0].Rows[0]["ChildCount"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
            }
            if (Convert.ToInt32(MailContent.Tables[0].Rows[0]["InfantCount"].ToString()) > 0)
            {
                strMailMsg = strMailMsg + "<tr>";
                strMailMsg = strMailMsg + "<td><b>Numbers of Infant: </b>" + MailContent.Tables[0].Rows[0]["InfantCount"].ToString();
                strMailMsg = strMailMsg + "</td>";
                strMailMsg = strMailMsg + "</tr>";
            }
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><b>Expacted Fare: </b>" + MailContent.Tables[0].Rows[0]["ExpactedPrice"].ToString();
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "<tr>";
            strMailMsg = strMailMsg + "<td><b>" + MailMSG + "</b>";
            strMailMsg = strMailMsg + "</td>";
            strMailMsg = strMailMsg + "</tr>";
            strMailMsg = strMailMsg + "</table>";
            MailDt = ObjST.GetMailingDetails("GroupBooking", Session["UID"].ToString());
            DataSet DSMail = new DataSet();
            DSMail = ObjGB.GroupRequestDetails("GroupBooking", "ConfigMail", "", "");
            if (MailDt.Tables[0].Rows.Count > 0 && DSMail.Tables[0].Rows.Count > 0)
            {
                for (int k = 0; k < DSMail.Tables[0].Rows.Count; k++)
                {
                    i = ObjGB.SendMail(DSMail.Tables[0].Rows[k]["ToEmail"].ToString(), MailDt.Tables[0].Rows[0]["MAILFROM"].ToString(), MailDt.Tables[0].Rows[0]["CC"].ToString(), "", MailDt.Tables[0].Rows[0]["SMTPCLIENT"].ToString(), MailDt.Tables[0].Rows[0]["UserID"].ToString(), MailDt.Tables[0].Rows[0]["Pass"].ToString(), strMailMsg, "Group Booking Details", "");
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLogTrace.WriteErrorLog(ex, "CustomerInfoIntlExecutiveMail");
        }
        return i;
    }
}
