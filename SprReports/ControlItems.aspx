﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ControlItems.aspx.cs" Inherits="ControlItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            height: 40px;
        }

        .auto-style3 {
            height: 22px;
        }
    </style>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8) || (charCode == 46)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function ValidateFunction() {
            if (document.getElementById("<%=txt_name.ClientID%>").value == "") {
                alert('Name can not be blank');
                document.getElementById("<%=txt_name.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_width.ClientID%>").value == "") {
                alert('Width can not be blank');
                document.getElementById("<%=txt_width.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_height.ClientID%>").value == "") {
                alert('Height can not be blank');
                document.getElementById("<%=txt_height.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_displaytime.ClientID%>").value == "") {
                alert('Display time can not be blank');
                document.getElementById("<%=txt_displaytime.ClientID%>").focus();
            return false;
        }
    }
    </script>
       <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Control Item </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                     <label for="exampleInputPassword1">Control Name :</label>
                                     <asp:TextBox ID="txt_name" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                 </div>
                                

                                        <div class="col-md-4">
                                              <div class="form-group">
                                     <label for="exampleInputPassword1">Width :</label>
                                   <asp:TextBox ID="txt_width" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"  class="form-control"></asp:TextBox>
                                    </div>
                                          </div>
                       </div>
                       

                                    
                                      <div class="row">
                                <div class="col-md-4">
                                     <div class="form-group">
                                     <label for="exampleInputPassword1">Height :</label>
                                   <asp:TextBox ID="txt_height" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server" class="form-control"></asp:TextBox>
                                      </div>
                                      </div>

                                            <div class="col-md-4">
                                                 <div class="form-group">
                                     <label for="exampleInputPassword1">Item Display At Time :</label>
                                  <asp:TextBox ID="txt_displaytime" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                                  </div>
                                          </div>

                                     
                                      <div class="row">
                                           <div class="form-group">
                                <div class="col-md-4">
                                     <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" CssClass="button buttonBlue" />
                                      </div>
                                          </div>
                                            </div>

    <%--<table class="auto-style1">
        <tr>
            <td>Control Name :
            <asp:TextBox ID="txt_name" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Width :
            <asp:TextBox ID="txt_width" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Height :<asp:TextBox ID="txt_height" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Item Display At Time :<asp:TextBox ID="txt_displaytime" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" />
            </td>
        </tr>
    </table>--%>
                                       </div>
                        </div>
                            </div>
                        </div>
                    </div>
              </asp:Content>

