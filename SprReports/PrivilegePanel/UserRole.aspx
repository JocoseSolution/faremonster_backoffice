﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="UserRole.aspx.cs" Inherits="Duplicate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Js/jquery-1.4.4.min.js"></script>
    <script src="Js/jquery-ui-1.8.8.custom.min.js"></script>

     <style>
      .error
      {
      color:red;
      padding-left:10px;
      display:none;
      }
           </style>


     <style>
      .error1
      {
      color:red;
      padding-left:10px;
      display:none;
      }
           </style>
    <script>
        $(document).ready(function () {
          
            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_usertypetxt").val();
              
                var length = data.length;

                if (length < 1) {
  
                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide(); //debugger;
                }


            });
        });
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <div>
        <table style="border-style: solid; border-color: inherit; border-width: medium; width: 399px; height: 146px; padding-right: 50px;">
            <tr>
                <td>User_id:</td>
                <td>
                    <input type="text" name="user_type" id="usertypetxt" runat="server" /><span id="text" class ="error">*</span></td>
            </tr>
            <tr>
                <td>Role:</td>
                <td>
                    <asp:DropDownList ID="Role" runat="server" Height="29px" Width="139px">
                    </asp:DropDownList><span id="dropdown" class ="error1">*</span></td>
            </tr>

            <tr>

                <td>


                    <asp:Button runat="server" Text="Submit" ID="Submit" OnClick="Submit_Click" />
                </td>
            </tr>

        </table>
    </div>
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>


    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="SNo"
 OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting"   >



        <Columns>
            <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="sno" runat="server" Text='<%# Eval("SNo") %>'></asp:Label>
                </ItemTemplate>


            </asp:TemplateField>
            <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblRoleid" runat="server" Text='<%# Eval("user_id") %>'></asp:Label>
                </ItemTemplate>


            </asp:TemplateField>
            <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>               
                </ItemTemplate>
                  <edititemtemplate>
                <asp:TextBox ID="txtRole" runat="server" Text='<%# Eval("Role") %>'></asp:TextBox>
                </edititemtemplate>

            </asp:TemplateField>

            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150" />
        </Columns>

        <HeaderStyle BackColor="Gray" ForeColor="#ffffff" />
        <RowStyle BackColor="#e7ceb6" />
    </asp:GridView>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {
                ////debugger;
                if ($.trim($("#<%=Role.ClientID%>").val()) == "0") {
                    //alert("value should be selected");
                    $('.error1').show();
                    $("#<%=Role.ClientID%>").focus();
                    return false;
                }
                else {
                    $('.error1').hide();
                }
               
             
            });
        });
    </script>


</asp:Content>

