﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
public partial class SprReports_PrivilegePanel_Page_User_Authorized : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!this.IsPostBack)
        {
          
            BindRole();
            BindPage_Name();
            GridView1.DataBind();

        }

     


    }


    public void BindRole()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand("BindRolePageAuthorised_PP");

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        con.Open();
        Role1.DataSource = cmd.ExecuteReader();
        Role1.DataTextField = "Role";
        Role1.DataValueField = "Role_id";
        Role1.DataBind();
        con.Close();
        Role1.Items.Insert(0, new ListItem("--Select Role--", "0"));
    }

    public void BindPage_Name()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand("BindRolePageName_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        con.Open();
        Page_name.DataSource = cmd.ExecuteReader();
        Page_name.DataTextField = "Page_name";
        Page_name.DataValueField = "Page_id";
        Page_name.DataBind();
        Page_name.Items.Insert(0, new ListItem("--Select PageName--", "0"));
        con.Close();
    }


    protected void Submit_Click(object sender, EventArgs e)
    {
        Pro bo = new Pro();
        string result = "";
        bo.Role_id = Convert.ToInt32(Role1.SelectedItem.Value);
        bo.Page_id = Convert.ToInt32(Page_name.SelectedItem.Value);
        Dao bl = new Dao();
        try
        {
            result = bl.InsertPage_Role(bo);
            if (result == "Insert")
            {

                Label1.InnerText = "Data submitted successfully";

                int role_id = Convert.ToInt32(Role1.SelectedItem.Value);
                BindGridview(role_id);
            }
            else
            {


                Label1.InnerText = "Data Already Exist...";
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            bo = null;
            bl = null;
        }


        //Role1.SelectedIndex = 0;   Page_name.SelectedIndex = 0;
    }

    private static DataTable GetData(string query)
    {

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = query;
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();
        sda.Fill(dt);
        con.Close();
        return dt;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void Role_SelectedIndexChanged(object sender, EventArgs e)
    {

        //ClientScript.RegisterStartupScript(this.GetType(), "showProgress('block')", "", true);
        //ClientScriptManager hh ;
        //hh.RegisterClientScriptBlock(this.GetType(), "showProgress('block')", "", true);      
       // Thread.Sleep(7878);

       // ClientScript.RegisterStartupScript(this.GetType(), "<script type='text/javascript'>showProgress('block');</script>","",true);



        //ScriptManager.RegisterStartupScript(this,this.GetType(), " ", "showProgress('block')", true);
       
       
        //Page page = HttpContext.Current.Handler as Page;
        //if (page != null)
        //{
        //    ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "showProgress('block');", true);
        //    //System.Threading.Thread.Sleep(1000);

        //    BindGridview(Convert.ToInt32(Role1.SelectedItem.Value));
        //    Label1.InnerText = "";
          
          
        //    ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "hideProgress('none');", false);
        //}
    
       
 
        //ClientScript.RegisterStartupScript(this.GetType(), "<script type='text/javascript'>showProgress('none');</script>", "", true);


       //// prg.Visible = false;
        //Page.RegisterStartupScript("dsd34", "<script type='text/javascript'>showProgress('none');</script>");


        BindGridview(Convert.ToInt32(Role1.SelectedItem.Value));
        Label1.InnerText = "";
    }
    protected void BindGridview(int id)
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);     
        SqlCommand cmd = new SqlCommand("GVPageAuthorizedSP_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@role_id", id);
        cmd.Connection = con;
        con.Open();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        GridView1.DataSource = ds;
        GridView1.DataBind();
    }



    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
        var abcde = e.CommandArgument.ToString().Split('~');
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            //using (SqlCommand cmd = new SqlCommand("PageAuthDeleteSp"))
            using (SqlCommand cmd = new SqlCommand("PageAuthDeleteSp_PP"))
            {

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Role", abcde[0]);
                cmd.Parameters.AddWithValue("@Pagename", abcde[1]);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                int role_id = Convert.ToInt32(Role1.SelectedItem.Value);
                BindGridview(role_id);
            }
        }
        }
        catch (Exception ex)
        {

        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        
    }
}
