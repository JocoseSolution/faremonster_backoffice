﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Text;
using System.IO.Compression;
using System.Xml.Linq;
public partial class SprReports_HoldPNR_HoldBookingStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            if(Session["UID"] == null)
                Response.Redirect("~/Login.aspx", false);
            if (!IsPostBack)
                showPNR.Visible = false;
    }
    protected void btn_result_Click(object sender, EventArgs e)
    {
        try
        {
            

             if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
            IntlDetails SelectDetails = new IntlDetails();
            SqlTransaction objDA = new SqlTransaction();
            SqlTransactionNew objSql = new SqlTransactionNew();
            DataSet FltDs = new DataSet(); DataSet dsCrd = new DataSet();
            STD.DAL.Credentials objCrd = new STD.DAL.Credentials(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            //FltDs = objDA.GetFltDtls(txt_OrderId.Text, Session["UID"].ToString());
            FltDs = USP_GetSelectedGalValue(txt_OrderId.Text, Session["UID"].ToString(), Session["User_Type"].ToString());
            dsCrd = objSql.GetCredentials("TQ");

            int hd = SelectDetails.UpdateFlightHeader(GDSPNR.Text.Trim(), AIRLINEPNR.Text.Trim(), "Ticketed", txt_OrderId.Text);
             if(hd > 0)
             {
                ErrorMsg.InnerText = "Record Updated";
                Status.Text = "Ticketed";
             }
            string[] SnoSplit = new string[] { "Ref!" };
            string[] SnoSplitDetails = FltDs.Tables[0].Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);

            if (SnoSplitDetails[4] == "false" && !GDSPNR.Text.Contains("-FQ"))
            {
                Hashtable TKTHT = new Hashtable();
                TKTHT = ITQairApiTiketting(dsCrd.Tables[0].Rows[0]["ServerUrlOrIP"].ToString().Replace("Pricing", "Ticket"), GDSPNR.Text, FltDs.Tables[0].Rows[0]["Searchvalue"].ToString(), txt_OrderId.Text, Session["UID"].ToString(), SnoSplitDetails);
                // ArrayList TktNoArray = (ArrayList) TKTHT("TktNoArray");
                try
                {
                    SqlTransactionDom objSqlDom = new SqlTransactionDom();
                    DataSet PaxDs = new DataSet();
                    PaxDs = objDA.GetPaxDetails(txt_OrderId.Text);
                    if (PaxDs.Tables.Count > 0)
                    {
                        for (int p = 0; p < TKTHT.Count; p++)
                        {
                            string PNameFromTbl = "", PNameFromTktArray = "", PNameFromTbl_UK = "";
                            if (PaxDs.Tables[0].Rows[p]["PaxType"].ToString().Trim().ToUpper() == "INF")
                                PNameFromTbl = PaxDs.Tables[0].Rows[p]["FName"].ToString() + PaxDs.Tables[0].Rows[p]["MName"].ToString() + PaxDs.Tables[0].Rows[p]["LName"].ToString().Replace(" ", "").ToUpper().Trim();
                            else
                            {
                                PNameFromTbl = PaxDs.Tables[0].Rows[p]["FName"].ToString() + PaxDs.Tables[0].Rows[p]["MName"].ToString() + PaxDs.Tables[0].Rows[p]["Title"].ToString() + PaxDs.Tables[0].Rows[p]["LName"].ToString().Replace(" ", "").ToUpper().Trim();
                                PNameFromTbl_UK = PaxDs.Tables[0].Rows[p]["Title"].ToString() + PaxDs.Tables[0].Rows[p]["MName"].ToString() + PaxDs.Tables[0].Rows[p]["FName"].ToString() + PaxDs.Tables[0].Rows[p]["LName"].ToString().Replace(" ", "").ToUpper().Trim();
                            }
                            if (TKTHT["TktNoArray"].ToString() != "AIRLINE")
                            {

                                for (int i = 0; i < TKTHT.Count; i++)
                                {
                                    string strTktNo = "";
                                    string[] strtktArray = TKTHT["TktNoArray"].ToString().Split('/');
                                    PNameFromTktArray = strtktArray[0].Replace(" ", "").ToUpper();
                                    strTktNo = strtktArray[1];
                                    if (PNameFromTbl == PNameFromTktArray || PNameFromTbl_UK == PNameFromTktArray)
                                    {
                                        objSqlDom.UpdateLedger_PaxId(Convert.ToInt32(PaxDs.Tables[0].Rows[i]["PaxId"]), strtktArray[1].ToString(), GDSPNR.Text);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                objSqlDom.UpdateLedger_PaxId(Convert.ToInt32(PaxDs.Tables[0].Rows[p]["PaxId"].ToString()), "", GDSPNR.Text);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);
                    ErrorMsg.InnerText = ex.Message;
                }
            }
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ErrorMsg.InnerText = ex.Message;
        }
    }

    public Hashtable ITQairApiTiketting(string URL, string GDSPNR, string Searchvalue, string Track_id, string User_id, string[] SnoSplitDetails)
    {
        Hashtable TKTHT = new Hashtable();
        ArrayList TktNoArray = new ArrayList();
        try
        {
            string BookingRequest = "{ \"SessionID\": \"" + SnoSplitDetails[0] + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + Searchvalue + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\" }";
            string BookingResponse = PostJsionXML(URL, "application/xml; charset=utf-8", BookingRequest);           
            if (BookingResponse.Contains("<Status>"))
            {
                XDocument ItqDocument = XDocument.Parse(BookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                if (ItqDocument.Element("BookingResponse").Element("Status").Value.Trim() == "Success")
                {

                    if (ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("BookingStatus").Value.ToUpper() == "Ticketed".ToUpper())
                    {

                        foreach (var paxtkt in ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("CustomerInfo").Element("PassengerTicketDetails").Elements("PassengerTicketDetails"))
                        {
                            TktNoArray.Add(paxtkt.Element("Title").Value + paxtkt.Element("FirstName").Value + paxtkt.Element("LastName").Value + "/" + paxtkt.Element("TicketNumber").Value);
                        }
                        TKTHT.Add("TktNoArray", TktNoArray);
                        //  AirPnr = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("FlightDetails").Element("AirLinePNR").Value;
                        GDSPNR = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("PNR").Value;
                        TKTHT.Add("PNRRTReq", BookingRequest);
                        TKTHT.Add("PNRRTRes", BookingResponse);
                        TKTHT.Add("AirPnr", GDSPNR);
                    }
                    else
                    {
                        TktNoArray.Add("AirLine is not able to make ETicket");
                        TKTHT.Add("TktNoArray", TktNoArray);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            TktNoArray.Add("AirLine is not able to make ETicket");
            TKTHT.Add("TktNoArray", TktNoArray);
            clsErrorLog.LogInfo(ex);
        }
        return TKTHT;
    }

    public static string Authenticate(string url, string userid, string password)
    {
        string request = "{\"UserName\": \"" + userid + "\",  \"Password\": \"" + password + "\"}";
        string response = PostJsionXML(url, "application/json; charset=utf-8", request);
        string[] sessions = response.Split(':');
        string session = sessions[1].Split(',')[0];
        return session.Replace("\"", "");
    }
    public static string PostJsionXML(string url, string AcceptType, string requestData)
    {
        string responseXML = string.Empty;
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            byte[] data = Encoding.UTF8.GetBytes(requestData);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Accept = AcceptType;
            request.Headers.Add("Accept-Encoding", "gzip");
            request.ReadWriteTimeout = 200000;
            request.Timeout = 200000;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(data, 0, data.Length);
            dataStream.Close();
            HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
            var rsp = webResponse.GetResponseStream();
            if (rsp == null)
            {
                //throw exception
            }
            if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
            {
                using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                {
                    responseXML = readStream.ReadToEnd();
                }
            }
            else
            {
                StreamReader reader = new StreamReader(rsp, Encoding.Default);
                responseXML = reader.ReadToEnd();
            }
        }
        catch (WebException webEx)
        {
            if (webEx != null)
            {
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                responseXML = new StreamReader(stream).ReadToEnd();
            }
            else
                responseXML = "<Errors>" + webEx.Message + "</Errors>";
        }
        catch (Exception ex)
        {
            responseXML = "<Errors>" + ex.Message + "</Errors>";
        }
        return responseXML;
    }
    protected void btn_Search_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                DataSet FltDs = new DataSet(); DataSet dsCrd = new DataSet();
                STD.DAL.Credentials objCrd = new STD.DAL.Credentials(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                SqlTransaction objDA = new SqlTransaction();
                SqlTransactionNew objSql = new SqlTransactionNew();
                FltDs = USP_GetSelectedGalValue(txt_OrderId.Text, Session["UID"].ToString(), Session["User_Type"].ToString());
                dsCrd = objSql.GetCredentials("TQ");

                if (FltDs.Tables.Count > 0 && dsCrd.Tables.Count > 0)
                {
                    if (FltDs.Tables[0].Rows.Count > 0 && dsCrd.Tables[0].Rows.Count > 0)
                    {
                        if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "TQ")
                        {
                            string[] SnoSplit = new string[] { "Ref!" };
                            string[] SnoSplitDetails = FltDs.Tables[0].Rows[0]["sno"].ToString().Split(SnoSplit, StringSplitOptions.RemoveEmptyEntries);

                            string sessionids = Authenticate(dsCrd.Tables[0].Rows[0]["LoginID"].ToString(), dsCrd.Tables[0].Rows[0]["UserID"].ToString(), dsCrd.Tables[0].Rows[0]["Password"].ToString());
                            string RetrieveRequest = "{ \"SessionID\": \"" + sessionids + "\", \"Key\": \"" + SnoSplitDetails[1] + "\", \"ReferenceNo\": \"" + FltDs.Tables[0].Rows[0]["Searchvalue"].ToString() + "\", \"Provider\": \"" + SnoSplitDetails[2] + "\" }";
                            string BookingResponse = PostJsionXML(dsCrd.Tables[0].Rows[0]["LoginPwd"].ToString().Replace("Availability", "GetBookingDetails"), "application/xml; charset=utf-8", RetrieveRequest);
                            if (BookingResponse.Contains("<Status>"))
                            {
                                XDocument ItqDocument = XDocument.Parse(BookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                GDSPNR.Text = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("PNR").Value;
                                AIRLINEPNR.Text = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("FlightDetails").Element("Segment").Element("AirLinePNR").Value;
                                Status.Text = ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("BookingStatus").Value;
                                showPNR.Visible = true;
                                foreach (var paxtkt in ItqDocument.Element("BookingResponse").Element("AirBookingResponse").Element("AirBooking").Element("CustomerInfo").Element("PassengerTicketDetails").Elements("PassengerTicketDetails"))
                                {
                                    if (paxtkt.Element("PaxTypeCode").Value.ToUpper() == "INF")
                                        ErrorMsg.InnerText += "<div  style=\"font:bold; color:red;\" >" + paxtkt.Element("FirstName").Value + " " + paxtkt.Element("LastName").Value + " / " + paxtkt.Element("TicketNumber").Value + " <br /></div>";
                                    else
                                        ErrorMsg.InnerHtml += "<div  style=\"font:bold; color:red;\" >" + paxtkt.Element("Title").Value + " " + paxtkt.Element("FirstName").Value + " " + paxtkt.Element("LastName").Value + " / " + paxtkt.Element("TicketNumber").Value + " <br /></div>";
                                }
                            }
                        }
                        else
                            ErrorMsg.InnerText = "Record not found";
                    }
                    else
                        ErrorMsg.InnerText = "Record not found";
                }
                else
                    ErrorMsg.InnerText = "Record not found";
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    public DataSet USP_GetSelectedGalValue(string orderid, string loginid, string user_Type)
    {
        DataAccess objDataAcess = new DataAccess(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        Hashtable paramHashtable = new Hashtable();
        paramHashtable.Clear();
        paramHashtable.Add("@TrackId", orderid);
        paramHashtable.Add("@UserId", loginid);
        paramHashtable.Add("@UserType", user_Type);
        return (DataSet)objDataAcess.ExecuteData<object>(paramHashtable, true, "GetSelectedFltDtls_Gal_Hold", 3);
    }

}