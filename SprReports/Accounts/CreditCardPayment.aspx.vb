﻿
Imports System.Security.Cryptography
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class SprReports_Accounts_CreditCardPayment
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom
    Private CCPG As New CCPaymentGateway
    Private con As New SqlConnection()
    Private adap As SqlDataAdapter
    Public action1 As String = String.Empty
    Public hash1 As String = String.Empty
    'Public txnid1 As String = String.Empty
    Protected Sub btnPay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPay.Click
        Try
            '' Dim ObjPayment As New ICICIPG.Service1
            Dim TrackID As String = "" 'GetUniqueKey(10)
            Dim rnd As New Random()
            Dim strHash As String = Generatehash512(rnd.ToString() + DateTime.Now)
            TrackID = strHash.ToString().Substring(0, 20)
            Dim dtPay As New DataTable()
            dtPay = STDom.GetAgencyDetails(Session("UID")).Tables(0)
            ' Dim strPayment As String = ObjPayment.UploadAmountHdfc(TrackID, dtPay.Rows(0)("Email").ToString(), dtPay.Rows(0)("Mobile").ToString(), dtPay.Rows(0)("Address").ToString(), txtAmount.Text.ToString(), Session("UID").ToString(), dtPay.Rows(0)("Agency_Name").ToString(), "http://115.119.118.48", "PGError.aspx", "PGResponse.aspx")
            'InsertCardDetails(txtName.Text.Trim(), TrackID, "HDFC", dtPay.Rows(0)("Email").ToString(), dtPay.Rows(0)("Mobile").ToString(), dtPay.Rows(0)("Address").ToString(), txtAmount.Text.ToString(), Session("UID").ToString(), dtPay.Rows(0)("Agency_Name").ToString(), Request.UserHostAddress)
            Dim dschk As New DataTable
            dschk = CCPG.CHECK_UPLOAD_PAYMENT(Session("UID"), "Transaction Successful", Convert.ToInt32(txtAmount.Text.Trim())).Tables(0)

            If (dschk.Rows(0)("AMT") = "True") Then
                Dim PGAmount As Double = CCPG.CalculateCCPercentage(Convert.ToInt32(txtAmount.Text.Trim()))
                Dim CCAmount As Integer
                CCAmount = Convert.ToInt32(txtAmount.Text.Trim()) '+ PGAmount
                Dim strReq As String = ""
                Dim strRes As String = ""
                Dim strPayment As String = ""
                Try
                    Dim dtCrd As New DataTable
                    dtCrd = CCPG.GetPgCrd("payu").Tables(0)
                    Dim hashVarsSeq As String()
                    Dim hash_string As String = String.Empty
                    ' generating txnid                   
                    hashVarsSeq = dtCrd.Rows(0)("hashSequence").Split("|"c) 'ConfigurationManager.AppSettings("hashSequence").Split("|"c)
                    ' spliting hash sequence from config
                    hash_string = ""
                    For Each hash_var As String In hashVarsSeq
                        If hash_var = "key" Then
                            hash_string = hash_string + dtCrd.Rows(0)("MERCHANT_KEY") ' ConfigurationManager.AppSettings("MERCHANT_KEY")
                            hash_string = hash_string & "|"c
                        ElseIf hash_var = "txnid" Then
                            hash_string = hash_string & TrackID
                            hash_string = hash_string & "|"c
                        ElseIf hash_var = "amount" Then
                            hash_string = hash_string & Convert.ToDecimal(CCAmount).ToString("g29")
                            hash_string = hash_string & "|"c
                        ElseIf hash_var = "productinfo" Then
                            hash_string = hash_string & "PAYMENTUPLOAD"
                            hash_string = hash_string & "|"c
                        ElseIf hash_var = "firstname" Then
                            hash_string = hash_string & txtFName.Text.Trim().ToString()
                            hash_string = hash_string & "|"c
                        ElseIf hash_var = "email" Then
                            hash_string = hash_string & dtPay.Rows(0)("Email").ToString()
                            hash_string = hash_string & "|"c
                        ElseIf hash_var = "udf1" Then
                            hash_string = hash_string & Session("UID").ToString()
                            hash_string = hash_string & "|"c
                           
                        Else

                            hash_string = hash_string & (If(Request.Form(hash_var) IsNot Nothing, Request.Form(hash_var), ""))
                            ' isset if else
                            hash_string = hash_string & "|"c
                        End If
                    Next

                    hash_string += dtCrd.Rows(0)("MERCHANT_PSWD") 'ConfigurationManager.AppSettings("SALT")
                    ' appending SALT
                    hash1 = Generatehash512(hash_string).ToLower()
                    'generating hash
                    ' setting URL
                    action1 = dtCrd.Rows(0)("ProviderUrl") + "/_payment" ' ConfigurationManager.AppSettings("PAYU_BASE_URL") + "/_payment"

                    Dim data As New System.Collections.Hashtable()
                    ' adding values in gash table for data post
                    data.Add("hash", hash1)
                    data.Add("txnid", TrackID)
                    data.Add("key", dtCrd.Rows(0)("MERCHANT_KEY")) 'ConfigurationManager.AppSettings("MERCHANT_KEY"))
                    Dim AmountForm As String = Convert.ToDecimal(CCAmount).ToString("g29")
                    ' eliminating trailing zeros                        
                    data.Add("amount", AmountForm)
                    data.Add("firstname", txtFName.Text.Trim())
                    data.Add("email", dtPay.Rows(0)("Email").ToString())
                    data.Add("phone", dtPay.Rows(0)("Mobile").ToString())
                    data.Add("productinfo", "PAYMENTUPLOAD")
                    data.Add("surl", dtCrd.Rows(0)("SuccessUrl")) '"http://localhost:16200/LNBB2B09DEC2013/SFAResponse.aspx")
                    data.Add("furl", dtCrd.Rows(0)("FailureUrl")) '"http://localhost:16200/LNBB2B09DEC2013/SFAResponse.aspx")
                    data.Add("lastname", txtLName.Text.Trim())
                    data.Add("curl", "")
                    data.Add("address1", dtPay.Rows(0)("Address").ToString())
                    data.Add("address2", "")
                    data.Add("city", dtPay.Rows(0)("City").ToString())
                    data.Add("state", dtPay.Rows(0)("State").ToString())
                    data.Add("country", "India")
                    data.Add("zipcode", dtPay.Rows(0)("zipcode").ToString())
                    data.Add("udf1", Session("UID").ToString())
                    data.Add("udf2", "")
                    data.Add("udf3", "")
                    data.Add("udf4", "")
                    data.Add("udf5", "")
                    data.Add("PG", "Wallet")
                    data.Add("Bankcode", "payuw")
                    strPayment = PreparePOSTForm(action1, data)
                    'Page.Controls.Add(New LiteralControl(strPayment))
                Catch ex As Exception
                End Try

                ' Dim strPayment As String = PostXml("http://www.kandharitravels.in/uploadpayment/service1.asmx", strReq)
                'Dim xd As New XmlDocument
                If strPayment <> "" Then
                    'xd.LoadXml(strPayment)
                    Dim i As Integer = CCPG.InsertCardDetails(txtFName.Text.Trim() & " " & txtLName.Text.Trim(), TrackID, "PayU", dtPay.Rows(0)("Email").ToString(), dtPay.Rows(0)("Mobile").ToString(), dtPay.Rows(0)("Address").ToString(), txtAmount.Text.ToString(), Session("UID").ToString(), dtPay.Rows(0)("Agency_Name").ToString(), Request.UserHostAddress)
                    If (i > 0) Then
                        'Response.Redirect(xd.InnerText)
                        Page.Controls.Add(New LiteralControl(strPayment))
                    Else
                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Can not process credit card payment . Please contact to administrator');", True)
                    End If
                Else
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('your payment exceed with Rs. " & Convert.ToDouble(txtAmount.Text.Trim()) - Convert.ToDouble(dschk.Rows(0)("AMT")) & " ');", True)
                End If
            Else
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('your payment exceed with Rs. " & Convert.ToDouble(txtAmount.Text.Trim()) - Convert.ToDouble(dschk.Rows(0)("AMT")) & " ');", True)
            End If


        Catch ex As Exception
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Payment gateway response slow. Please try again later');", True)
        End Try

    End Sub
    Private Function PreparePOSTForm(ByVal url As String, ByVal data As System.Collections.Hashtable) As String
        ' post form
        'Set a name for the form
        Dim formID As String = "PostForm"
        'Build the form using the specified data to be posted.
        Dim strForm As New StringBuilder()
        strForm.Append("<form id=""" & formID & """ name=""" & formID & """ action=""" & url & """ method=""POST"">")

        For Each key As System.Collections.DictionaryEntry In data

            strForm.Append("<input type=""hidden"" name=""" & Convert.ToString(key.Key) & """ value=""" & Convert.ToString(key.Value) & """>")
        Next


        strForm.Append("</form>")
        'Build the JavaScript which will do the Posting operation.
        Dim strScript As New StringBuilder()
        strScript.Append("<script language='javascript'>")
        strScript.Append("var v" & formID & " = document." & formID & ";")
        strScript.Append("v" & formID & ".submit();")
        strScript.Append("</script>")
        'Return the form and the script concatenated.
        '(The order is important, Form then JavaScript)
        Return strForm.ToString() + strScript.ToString()
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            Dim DivPayment As String = Request("TrackId")
            If (DivPayment = "" Or DivPayment Is Nothing) Then
                Div_PaymentId.Visible = True
                Div_Response.Visible = False
                Div_ResponseFail.Visible = False
                PgError.Visible = False
            ElseIf (DivPayment <> "" AndAlso DivPayment <> "Err" AndAlso Request("Type") = "PGRes") Then
                Div_PaymentId.Visible = False
                Div_Response.Visible = True
                Div_ResponseFail.Visible = False
                PgError.Visible = False
                td_trackid.InnerText = DivPayment
                Dim dt As New DataTable
                dt = CCPG.GetAgentIdByCCTrackId(DivPayment).Tables(0)
                td_PaymentId.InnerText = dt.Rows(0)("PaymentId").ToString()
                td_Amount.InnerText = dt.Rows(0)("OriginalAmount").ToString()
                td_creditamount.InnerText = dt.Rows(0)("Amount").ToString()
                td_surcharge.InnerText = Convert.ToInt32(dt.Rows(0)("OriginalAmount").ToString()) - Convert.ToInt32(dt.Rows(0)("Amount").ToString())
            ElseIf (DivPayment = "Err") Then
                Div_PaymentId.Visible = False
                Div_Response.Visible = False
                PgError.Visible = False
                Div_ResponseFail.Visible = True
            ElseIf (DivPayment <> "" AndAlso DivPayment <> "Err" AndAlso Request("Type") = "PGResError") Then
                Div_PaymentId.Visible = False
                Div_Response.Visible = False
                PgError.Visible = True
                Div_ResponseFail.Visible = False
                Dim dt As New DataTable
                dt = CCPG.GetAgentIdByCCTrackId(DivPayment).Tables(0)
                td_Amt.InnerText = dt.Rows(0)("Amount").ToString()
                td_payment.InnerText = dt.Rows(0)("PaymentId").ToString()
                td_track.InnerText = DivPayment
                td_PStatus.InnerText = dt.Rows(0)("ErrorText").ToString()
            End If


        Catch ex As Exception

        End Try


    End Sub
    Public Function GetUniqueKey(ByVal maxSize As Integer) As String
        Dim chars As Char() = New Char(61) {}
        chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray()
        Dim data As Byte() = New Byte(0) {}
        Dim crypto As New RNGCryptoServiceProvider()
        crypto.GetNonZeroBytes(data)
        data = New Byte(maxSize - 1) {}
        crypto.GetNonZeroBytes(data)
        Dim result As New StringBuilder(maxSize)
        For Each b As Byte In data
            result.Append(chars(b Mod (chars.Length)))
        Next
        Return result.ToString()
    End Function
    'Public Sub InsertCardDetails(ByVal Name As String, ByVal TrackId As String, ByVal PaymentGateway As String, ByVal Email As String, ByVal Mobile As String, ByVal Address As String, ByVal Amount As Double, ByVal AgentId As String, ByVal AgencyName As String, ByVal Ip As String)
    '    If con.State = ConnectionState.Open Then
    '        con.Close()
    '    End If
    '    Dim DS As New DataSet()
    '    con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
    '    adap = New SqlDataAdapter("InsertCreditCardDetails", con)
    '    adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '    adap.SelectCommand.Parameters.AddWithValue("@Name", Name)
    '    adap.SelectCommand.Parameters.AddWithValue("@TrackId", TrackId)
    '    adap.SelectCommand.Parameters.AddWithValue("@PaymentGateway", PaymentGateway)
    '    adap.SelectCommand.Parameters.AddWithValue("@Email", Email)
    '    adap.SelectCommand.Parameters.AddWithValue("@Mobile", Mobile)
    '    adap.SelectCommand.Parameters.AddWithValue("@Address", Address)
    '    adap.SelectCommand.Parameters.AddWithValue("@Amount", Amount)
    '    adap.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId)
    '    adap.SelectCommand.Parameters.AddWithValue("@Status", "Request")
    '    adap.SelectCommand.Parameters.AddWithValue("@AgencyName", AgencyName)
    '    adap.SelectCommand.Parameters.AddWithValue("@Ip", Ip)
    '    adap.Fill(DS)

    'End Sub
    Private Function PostXml(ByVal url As String, ByVal xml As String) As String
        Dim bytes As Byte() = UTF8Encoding.UTF8.GetBytes(xml)
        Dim strResult As String = String.Empty
        Try
            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
            request.Method = "POST"
            request.ContentLength = bytes.Length
            request.ContentType = "text/xml"
            request.KeepAlive = False
            request.Timeout = 3600 * 1000
            request.ReadWriteTimeout = 3600 * 1000

            Using requestStream As Stream = request.GetRequestStream()
                requestStream.Write(bytes, 0, bytes.Length)
            End Using
            Using response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
                If response.StatusCode <> HttpStatusCode.OK Then
                    Dim message As String = [String].Format("POST failed. Received HTTP {0}", response.StatusCode)
                    Throw New ApplicationException(message)
                Else
                    Dim xmldoc As New XmlDocument()
                    Dim reader As StreamReader = Nothing
                    Dim responseStream As Stream = response.GetResponseStream()
                    reader = New StreamReader(responseStream)
                    strResult = reader.ReadToEnd()
                    response.Close()
                    responseStream.Close()
                    reader.Close()
                End If
            End Using
        Catch ex As Exception
        End Try
        Return strResult
    End Function
    Protected Sub btn_Calculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Calculate.Click
        Try
            Dim a As Double = CCPG.CalculateCCPercentage(Convert.ToInt32(txtAmount.Text.Trim()))
            lbl_ccamt.Text = a
            Dim srvtax As Double = 0
            srvtax = Math.Round((a * 12.36 / 100), 2)
            lbl_stax.Text = srvtax
            td_Transchrg.Visible = True
        Catch ex As Exception

        End Try

    End Sub

    Public Function Generatehash512(ByVal text As String) As String

        Dim message As Byte() = Encoding.UTF8.GetBytes(text)

        Dim UE As New UnicodeEncoding()
        Dim hashValue As Byte()
        Dim hashString As New SHA512Managed()
        Dim hex As String = ""
        hashValue = hashString.ComputeHash(message)
        For Each x As Byte In hashValue
            hex += [String].Format("{0:x2}", x)
        Next
        Return hex

    End Function
End Class
