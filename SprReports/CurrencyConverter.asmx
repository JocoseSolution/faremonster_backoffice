﻿<%@ WebService Language="C#" Class="CurrencyConverter" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web.Security;
using HotelShared;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class CurrencyConverter  : System.Web.Services.WebService {

    [WebMethod()]
    public string GetCurrency(string CountryCode)
    {
        string Currency = "INR";
        try
        {
            HotelDAL.HotelDA objda = new HotelDAL.HotelDA();

            CurrencyRateSetting objexchagerate = new CurrencyRateSetting();
            objexchagerate.ReqType = "SelectCurrency";
            objexchagerate.CountryCode = CountryCode;
           // objexchagerate = CurrancyDetails(objexchagerate);
            if (objexchagerate.CurrancyList != null)
                if (objexchagerate.CurrancyList.Count > 0)
                    Currency = objexchagerate.CurrancyList[0].CurrancyCode;

        }
        catch (Exception ex)
        {

        }
        return Currency;
    }
   [WebMethod(EnableSession = true)]
    public CurrencyRateSetting CurrencyExchangeRate(CurrencyRateSetting CRS)
    {
        List<CurrencyRate> CRList = new List<CurrencyRate>();
        try
        {
            if (CRS.CountryName == null)
            {
                CRS.CountryName = "";
            }
            if (CRS.CountryCode == null)
            {
                CRS.CountryCode = "";
            }
            if (CRS.CurrancyCode == null)
            {
                CRS.CurrancyCode = "";
            }
            CRS.UpdatedBy = HttpContext.Current.Session["UID"].ToString();

            string Result = "";
            if (CRS.ReqType == null)
                CRS.ReqType = "ReportCurrency";
            else if (CRS.ReqType == "InsertCurrency")
            {
                CRS.UpdatedBy = HttpContext.Current.Session["UID"].ToString();
                CRS.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                Result = InsertUpdateCurrancyDetails(CRS);
            }
           // TempData["Message"] = Result;
            CRS.ReqType = "ReportCurrency";
           // CRS = CurrancyDetails(CRS);
          
        }
        catch (Exception ex)
        {
           
           // TempData["Message"] = "ERROR";
        }
        return  CRS;
    }

    [WebMethod(EnableSession = true)]
    public string CurrencyExchangeRateUpdate(int Id, decimal amt, string ReqType)
    {
        string Result = "";
        List<CurrencyRate> CRList = new List<CurrencyRate>();
        CurrencyRateSetting CRS = new CurrencyRateSetting();
        try
        {
            
            CRS.IDS = Id;
            CRS.ExchangeRate = amt;
            CRS.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            CRS.UpdatedBy = HttpContext.Current.Session["UID"].ToString();
            CRS.ReqType = ReqType;

            if (CRS.ReqType == "UpdateCurrency" || CRS.ReqType == "DeleteCurrency")
                Result = InsertUpdateCurrancyDetails(CRS);
        }
        catch (Exception ex) {  }
        return Result;
    }
    [WebMethod(EnableSession = true)]
    public List<CurrencyRate> CurrancyDetails(string CountryName, string CountryCode, string CurrancyCode, string ReqType)
    {
        SqlDatabase DBhelperfws = new SqlDatabase(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
	    
        List<CurrencyRate> currancyinfo = new List<CurrencyRate>();
        SqlCommand sqlcmd = new SqlCommand("SP_InsertCurrencyDetails");
        try
        {
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@CountryName", CountryName);
            sqlcmd.Parameters.AddWithValue("@CountryCode", CountryCode);
            sqlcmd.Parameters.AddWithValue("@CurrancyCode", CurrancyCode);
            sqlcmd.Parameters.AddWithValue("@ExchangeRate", 0);
            sqlcmd.Parameters.AddWithValue("@UpdateBy", HttpContext.Current.Session["UID"].ToString());
            sqlcmd.Parameters.AddWithValue("@Reqtype", ReqType);
            sqlcmd.Parameters.AddWithValue("@IPAddress", HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
            sqlcmd.Parameters.AddWithValue("@ID", 0);
            var dbr = DBhelperfws.ExecuteReader(sqlcmd);

            while (dbr.Read())
            {
                currancyinfo.Add(new CurrencyRate
                {
                    CountryName = dbr["CountryName"] != null ? dbr["CountryName"].ToString() : "",
                    CountryCode = dbr["CountryCode"] != null ? dbr["CountryCode"].ToString() : "",
                    CurrancyCode = dbr["CurrancyCode"] != null ? dbr["CurrancyCode"].ToString() : "",
                    CurrancyName = dbr["CurrencyName"] != null ? dbr["CurrencyName"].ToString() : "",
                    ExchangeRate = dbr["ExchangeRate"] != null ? Convert.ToDecimal(dbr["ExchangeRate"]) : 0,
                    IDS = Convert.ToInt32(dbr["id"]),
                    UpdateDate = dbr["UpdateDate"].ToString(),
                    
                });
            }
        }
        catch (Exception ex)
        {

        }
        finally { sqlcmd.Dispose(); }
        return currancyinfo;
    }
    [WebMethod()]
    public string InsertUpdateCurrancyDetails(CurrencyRateSetting objcurrancy)
    {
        SqlDatabase DBhelperfws = new SqlDatabase(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        string i = "";
        SqlCommand sqlcmd = new SqlCommand("SP_InsertCurrencyDetails");
        try
        {
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@CountryName", objcurrancy.CountryName);
            sqlcmd.Parameters.AddWithValue("@CountryCode", objcurrancy.CountryCode);
            sqlcmd.Parameters.AddWithValue("@CurrancyCode", objcurrancy.CurrancyCode);
            sqlcmd.Parameters.AddWithValue("@ExchangeRate", objcurrancy.ExchangeRate);
            sqlcmd.Parameters.AddWithValue("@UpdateBy", objcurrancy.UpdatedBy);
            sqlcmd.Parameters.AddWithValue("@Reqtype", objcurrancy.ReqType);
            sqlcmd.Parameters.AddWithValue("@IPAddress", objcurrancy.IPAddress);
            sqlcmd.Parameters.AddWithValue("@ID", objcurrancy.IDS);
            i = DBhelperfws.ExecuteScalar(sqlcmd).ToString();
        }
        catch (Exception ex)
        {

        }
        finally { sqlcmd.Dispose(); }
        return i;
    }

    
    //public class CurrencyRate
    //{
    //    public string CountryCode { get; set; }
    //    public string CountryName { get; set; }
    //    public string CurrancyCode { get; set; }
    //    public string CurrancyName { get; set; }
    //    public string CurrancySymble { get; set; }
    //    public decimal ExchangeRate { get; set; }
    //    public string UpdateDate { get; set; }
    //    public int IDS { get; set; }
    //}
    //public class CurrencyRateSetting
    //{
    //    public string Code { get; set; }
    //    public string CountryName { get; set; }
    //    public string CountryCode { get; set; }
    //    public string CurrancyCode { get; set; }
    //    public decimal ExchangeRate { get; set; }
    //    public string UpdateDate { get; set; }
    //    public string UpdatedBy { get; set; }
    //    public string IPAddress { get; set; }
    //    public string ReqType { get; set; }
    //    public int IDS { get; set; }
    //    public List<CurrencyRate> CurrancyList { get; set; }
    //}
}