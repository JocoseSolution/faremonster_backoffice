﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Xml
Imports System.IO
Imports HotelShared

Partial Class HotelBookingConfirmation
    Inherits System.Web.UI.Page
    Dim SearchDetails As New HotelBooking

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx", False)
        End If

        If Not Page.IsPostBack Then
            Dim sBookingStatus As String = "", sConfId As String = ""
            SearchDetails = CType(Session("HotelBooking"), HotelBooking)
            Try
                If SearchDetails.BookingID = "" Then
                    HoldBookingTD.Visible = True
                End If

                lblStatus.InnerText = SearchDetails.Status
                lblbookingID.InnerText = SearchDetails.BookingID
                lblBookingDate.InnerText = Now.Date.ToString("dd-MMM-yyyy")
                lblTotal2.InnerText = "INR " & SearchDetails.TotalRoomrate.ToString() & " /-"
                lblHotelName.Text = Session("HtlName").ToString()
                lblRoomType.Text = SearchDetails.RoomName
                lblcheckin.Text = Session("sCheckIn1")
                lblCheckout.Text = Session("sCheckOut1")
                Try
                    If SearchDetails.ExtraRoom = 0 Then
                        lblRoom.Text = SearchDetails.NoofRoom
                    Else
                        lblRoom.Text = (SearchDetails.NoofRoom + SearchDetails.ExtraRoom).ToString()
                    End If
                Catch ex As Exception
                    lblRoom.Text = SearchDetails.NoofRoom
                End Try

                lblAdults.Text = SearchDetails.TotAdt
                lblChilds.Text = SearchDetails.TotChd
                TDGuestName.InnerText = SearchDetails.PGTitle & " " & SearchDetails.PGFirstName & " " & SearchDetails.PGLastName
                TDGuestMobile.InnerText = SearchDetails.PGContact
                lblEmail.Text = SearchDetails.PGEmail
                htlPhone.InnerText = SearchDetails.HotelContactNo
                htlFax.InnerText = SearchDetails.HotelAddress
                TdOrderid.InnerText = SearchDetails.Orderid
                lblnight.Text = SearchDetails.NoofNight.ToString()

                Dim HotelPolicy As String = "<span style='font-size:13px;font-weight: bold;'>HOTEL POLICIES</span><span>"
                HotelPolicy += "<li> As per Government regulations, it is mandatory for all guests above 18 years of age to carry a valid photo identity card & address proof at the time of check-in. Please note that failure to abide by this can result with the hotel denying a check-in. Hotels normally do not provide any refund for such cancellations.</li>"
                HotelPolicy += "<li> The standard check-in and check-out times are 12 noon. Early check-in or late check-out is subject to hotel availability and may also be chargeable by the hotel. Any early check-in or late check-out request must be directed to and reconfirmed with the hotel directly.</li>"
                HotelPolicy += "<li> Failure to check-in to the hotel, will attract the full cost of stay or penalty as per the hotel cancellation policy.</li>"
                HotelPolicy += "<li> All additional charges other than the room charges and inclusions as mentioned in the booking voucher are to be borne and paid separately during check-out. Please make sure that you are aware of all such charges that may comes as extras. Some of them can be WiFi costs, Mini Bar, Laundry Expenses, Telephone calls, Room Service, Snacks etc.</li>"
                HotelPolicy += "<li> Any changes or booking modifications are subject to availability and charges may apply as per the hotel policies.</li></span>"

                lblRules.Text = HttpUtility.HtmlDecode(SearchDetails.HotelPolicy + HotelPolicy)
                BookingCopy.Text = Session("BookingCopy").ToString()
                SearchDetails.HtlCode = "Sorry Back Button Press"
            Catch ex As Exception
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
            End Try
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, Session("UID").ToString())
        End Try
    End Sub
End Class


